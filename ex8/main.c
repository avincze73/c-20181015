/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 16, 2018, 9:19 AM
 */

#include <stdio.h>
#include <stdlib.h>

int** getNumbers() {
    int** result = malloc(sizeof (int*)*4);
    
    *result = malloc(sizeof (int));
    **result = 1;

    *(result + 1) = malloc(sizeof (int)*2);
    **(result + 1) = 2;
    *(*(result + 1) + 1) = 3;

    *(result + 2) = malloc(sizeof (int)*3);
    **(result + 2) = 4;
    *(*(result + 2) + 1) = 5;
    *(*(result + 2) + 2) = 6;

    *(result + 3) = malloc(sizeof (int)*4);
    **(result + 3) = 7;
    *(*(result + 3) + 1) = 8;
    *(*(result + 3) + 2) = 9;
    *(*(result + 3) + 3) = 10;


    return result;
}

void freeNumbers(int** arg) {
    for (int i = 0; i < 4; ++i) {
        free(*(arg + i));
    }
    free(arg);
}

/*
 * 
 */
int main(int argc, char** argv) {
    int** numbers = getNumbers();
    printf("%d", numbers[3][3]);
    freeNumbers(numbers);
    return (EXIT_SUCCESS);
}

