/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 16, 2018, 11:42 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int compareStrings(const void* s1, const void* s2)
{
    return strcmp((const char*) s1, (const char*) s2);
}

bool areAnagrams(const char* s1, const char* s2)
{
    if(strlen(s1) != strlen(s2))
    {
        return false;
    }
    char ss1[strlen(s1)+1];
    strcpy(ss1, s1);
    qsort(ss1, strlen(ss1), sizeof(char), compareStrings);
    
    char ss2[strlen(s2)+1];
    strcpy(ss2, s2);
    qsort(ss2, strlen(ss2), sizeof(char), compareStrings);
    
    return strcmp(ss1, ss2) == 0;
}

/*
 * 
 */
int main(int argc, char** argv) {

    printf("%s", areAnagrams("abcd", "bacd")?"true":"false");
    return (EXIT_SUCCESS);
}

