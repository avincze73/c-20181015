/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 16, 2018, 10:48 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int accum(int* array, int length, int init)
{
    for(int i = 0; i < length; ++i)
    {
        init += i[array];
    }
    return init;
}

int accum1(int* array, int length, int init,
        int(*operation)(int, int))
{
    for(int i = 0; i < length; ++i)
    {
        init = operation(init, *(array+i));
    }
    return init;
}

int add(int a, int b)
{
    return a + b;
}

int multiply(int a, int b)
{
    return a * b;
}

int accum2(int* begin, int* end, int init,
        int(*operation)(int, int),
        bool(*predicate)(int))
{
    for(;begin!=end;begin++)
    {
        if(predicate(*begin))
        {
            init = operation(*begin, init);
        }
    }
    return init;
}

bool even(int i)
{
    return i % 2 == 0;
}

bool positive(int i)
{
    return i > 0;
}
/*
 * 
 */
int main(int argc, char** argv) {

    int array[] = {1,2,3,4,5,6,7,8,9,10};
    printf("%d\n", accum(array, 10, 0));
    
    printf("%d\n", accum1(array, 10, 0, add));
    printf("%d\n", accum1(array, 10, 1, &multiply));
    
    printf("%d\n", accum2(array, array + 10, 0, add, even));
    printf("%d\n", accum2(array, array + 10, 1, multiply, positive));
    
    
    return (EXIT_SUCCESS);
}

