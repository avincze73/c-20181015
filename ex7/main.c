/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 15, 2018, 3:32 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void myReplace(char* array) {
    for (int i = 0; i < strlen(array); ++i) {
        if (!isalpha(i[array])) {
            i[array] = ' ';
        }
    }
}

void readFromTextFile(const char* fileName) {
    FILE* fp;
    char buffer[255];

    fp = fopen(fileName, "r");

    while (fgets(buffer, 255, (FILE*) fp)) {
        printf("%s", buffer);
    }

    fclose(fp);
}

/*
 * 
 */
int main(int argc, char** argv) {
    char array[] = "a123b456c789";
    myReplace(array);
    printf("\"%s\"", array);
    readFromTextFile("d:\\Trainings\\dictionary.txt");
    return (EXIT_SUCCESS);
}

