/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 16, 2018, 1:33 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void readFromTextFile(const char* fileName) {
    FILE* fp;
    char buffer[255];

    fp = fopen(fileName, "r");

    while (fgets(buffer, 255, (FILE*) fp)) {
        for(int i=0; i<255;i++)
        {
            if(buffer[i]=='\n'){
                buffer[i] = '\0';
                break;
            }
        }
        printf("%s", buffer);
    }

    fclose(fp);
}

int compareStrings(const void* s1, const void* s2)
{
    return strcmp((const char*) s1, (const char*) s2);
}


bool areAnagrams(const char* s1, const char* s2) {
    if (strlen(s1) != strlen(s2)) {
        return false;
    }
    char ss1[strlen(s1) + 1];
    strcpy(ss1, s1);
    qsort(ss1, strlen(ss1), sizeof (char), compareStrings);

    char ss2[strlen(s2) + 1];
    strcpy(ss2, s2);
    qsort(ss2, strlen(ss2), sizeof (char), compareStrings);

    return strcmp(ss1, ss2) == 0;
}

void exE(const char* word) {
    FILE* fp;
    char buffer[255];

    fp = fopen("d:\\Trainings\\dictionary.txt", "r");

    while (fgets(buffer, 255, (FILE*) fp)) {
        for(int i=0; i<255;i++)
        {
            if(buffer[i]=='\r'){
                buffer[i] = '\0';
                break;
            }
        }
        //printf("%s", buffer);
        if (areAnagrams(buffer, word)) {
            printf("%s\n", buffer);
        }
    }

    fclose(fp);
}

typedef struct 
{
    char sorted[25];
    int count;
    char words[6][25];
} Item;

void exF() {
    FILE* fp;
    char buffer[255];
    //Item container[15000] = {};
    Item* container = calloc(22000,sizeof(Item));

    fp = fopen("d:\\Sprint\\CProgramming\\trainings\\20181015\\dictionary.txt", "r");
    int top = 0;

    while (fgets(buffer, 255, (FILE*) fp)) {
        for(int i=0; i<255;i++)
        {
            if(buffer[i]=='\r'){
                buffer[i] = '\0';
                break;
            }
        }
        char sorted[strlen(buffer)+1];
        strcpy(sorted, buffer);
        qsort(sorted, strlen(sorted), sizeof(char),compareStrings );
        bool found = false;
        for(int i = 0; i <top; i++)
        {
            if(strcmp(container[i].sorted, sorted)==0)
            {
                found = true;
                strcpy(container[i].words[container[i].count], buffer);
                container[i].count++;
                break;
            }
        }
        if(!found)
        {
            strcpy(container[top].sorted, sorted);
            strcpy(container[top].words[container[top].count], buffer);
            container[top].count++;
            top++;
        }
        
        if(top == 22000) break;
    }

    fclose(fp);
    
    for(int i = 0; i < top; ++i)
    {
        if(container[i].count <= 4) continue;
        printf("%s: ", container[i].sorted);
        for(int j=0; j<=container[i].count; j++)
        {
             printf("%s  ", container[i].words[j]);
        }
        printf("\n");
    }
    
    free(container);
}



/*
 * 
 */
int main(int argc, char** argv) {

    //exE("secure");
    exF();
    return (EXIT_SUCCESS);
}

