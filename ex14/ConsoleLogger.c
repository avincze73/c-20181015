/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "ConsoleLogger.h"
#include <stdio.h>

void console_logger(const char* message, severity sev)
{
    printf("%s\t%s\t%s\n", "CONSOLE", message, sev==INFO?"INFO":"ERROR");
}