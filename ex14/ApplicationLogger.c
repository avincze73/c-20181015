/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * https://gitlab.com/avincze73/c-20181015.git
 */

#include "ApplicationLogger.h"
#include <stdlib.h>
#include <stdbool.h>

static logger_inteface* registry = NULL;
static int size = 0;
static int capacity = 0;

void LOG(const char* message, severity sev) {
    for (int i = 0; i < size; ++i) {
        if (registry[i] != NULL) {
            (*(registry + i))(message, sev);
        }
    }
}

void attach(logger_inteface logger) {
    bool found = false;
    for (int i = 0; i < size; ++i) {
        if (registry[i] == logger) {
            found = true;
            break;
        }
    }
    if (!found) {
        if (size == capacity) {
            capacity = 2 * size + 1;
            logger_inteface* temp = malloc(sizeof (logger_inteface) * capacity);
            for (int i = 0; i < size; i++) {
                temp[i] = registry[i];
            }
            free(registry);
            registry = temp;
        }
        registry[size++] = logger;
    }
}

void detach(logger_inteface logger) {
    for (int i = 0; i < size; ++i) {
        if (i[registry] == logger) {
            i[registry] = NULL;
        }
    }
}