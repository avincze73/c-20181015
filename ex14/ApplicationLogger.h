/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ApplicationLogger.h
 * Author: avincze
 *
 * Created on October 18, 2018, 10:05 AM
 */

#ifndef APPLICATIONLOGGER_H
#define APPLICATIONLOGGER_H

typedef enum {INFO, ERROR} severity;
typedef void (*logger_inteface)(const char*, severity);

void LOG(const char* message, severity sev);
void attach(logger_inteface logger);
void detach(logger_inteface logger);


#endif /* APPLICATIONLOGGER_H */

