/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EmailLogger.h
 * Author: avincze
 *
 * Created on October 18, 2018, 10:17 AM
 */

#ifndef EMAILLOGGER_H
#define EMAILLOGGER_H

#include "ApplicationLogger.h"
void email_logger(const char*, severity);

#endif /* EMAILLOGGER_H */

