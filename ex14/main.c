/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2018. október 18., 10:04
 */

#include <stdio.h>
#include <stdlib.h>
#include "ApplicationLogger.h"
#include "ConsoleLogger.h"
#include "EmailLogger.h"
#include "FileLogger.h"


/*
 * 
 */
int main(int argc, char** argv) {
    attach(console_logger);
    LOG("first message", INFO);
    
    printf("\n");
    attach(file_logger);
    LOG("second message", INFO);
    
    printf("\n");
    attach(email_logger);
    LOG("third message", INFO);
    
    printf("\n");
    detach(email_logger);
    LOG("fourth message", INFO);
    
    printf("\n");
    detach(file_logger);
    LOG("fifth message", INFO);
    
    printf("\n");
    detach(console_logger);
    LOG("sixth message", INFO);
    return (EXIT_SUCCESS);
}

