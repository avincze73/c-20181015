/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConsoleLogger.h
 * Author: avincze
 *
 * Created on October 18, 2018, 10:10 AM
 */

#ifndef CONSOLELOGGER_H
#define CONSOLELOGGER_H
#include "ApplicationLogger.h"
void console_logger(const char*, severity);

#endif /* CONSOLELOGGER_H */