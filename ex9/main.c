/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 16, 2018, 10:03 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void getDays(char*** days)
{
    const char* myDays[] = {"sunday", "monday", "tuesday", "wednesday","thursday", "friday", "saturday"};
    *days = malloc(sizeof(char*)*7);
    for(int i = 0; i < 7; i++)
    {
        (*days)[i] = malloc(sizeof(char)*(strlen(myDays[i])+1));
        strcpy((*days)[i], myDays[i]);
    }
}

void freeDays(char** days)
{
    for(int i = 0 ; i < 7; i++)
    {
        free(*(days+i));
    }
    free(days);
}


void printDays(char** days)
{
    for(int i = 0 ; i < 7; i++)
    {
        printf("%s\n", days[i]);
    }
    free(days);
}

/*
 * 
 */
int main(int argc, char** argv) {

    char**  days = NULL;
    getDays(&days);
    printDays(days);
    freeDays(days);
    return (EXIT_SUCCESS);
}

