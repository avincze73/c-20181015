/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on September 24, 2018, 2:02 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main(int argc, char** argv) {
    int k = 0, array[32] = {};
    int number = 33;
    while (number > 0) {
        array[k++] = number % 2;
        number /= 2;
    }
    for (int i = 31; i >= 0; i--) {
        printf("%d", array[i]);
        if (!(i % 8)) {
            printf("%s", " ");
        }
    }
    printf("\n");


    return (EXIT_SUCCESS);
}

