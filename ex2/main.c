/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 15, 2018, 11:25 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool isCompleteSquare(int number);
/*
 * 
 */
int main(int argc, char** argv) {

    int number = 24;
    printf("%d: %s", number, 
            isCompleteSquare(number)?"true":"false");
    return (EXIT_SUCCESS);
}

bool isCompleteSquare(int number){
    for(int i = 0; i*i <= number; i++)
    {
        if(i*i == number) return true;
    }
    return false;
}

