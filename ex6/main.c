/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 15, 2018, 3:32 PM
 */

#include <stdio.h>
#include <stdlib.h>

void analyse(int* array, int length,
        int* positives, int* negatives, int* zeros);


/*
 * 
 */
int main(int argc, char** argv) {

    int positives = 0, negatives = 0, zeros = 0;
    int array[] = {1,2,3,4,-1,-2,-3,-4, 0,0,0,0};
    analyse(array, 12, &positives, &negatives, &zeros);
    printf("%d, %d, %d", positives, negatives, zeros);
    return (EXIT_SUCCESS);
}

void analyse(int* array, int length,
        int* positives, int* negatives, int* zeros)
{
    for(int i = 0; i < length; ++i){
        if (i[array] > 0) {
            (*positives)++;
        } else if (array[i]<0) {
            (*negatives)++;
        } else {
            (*zeros)++;
        }
    }
}
