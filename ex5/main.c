/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on September 24, 2018, 2:02 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main(int argc, char** argv) {
    int harmadik = 0, index = -1, t[3] = {};
    for (;;) {
        scanf("%d", &t[index = (index + 1) % 3]);
        if (!t[index]) break;
        if (index == 2) harmadik = 1;
        if (harmadik &&
        (t[index] == (t[(index + 1) % 3] + t[(index + 2) % 3]))) 
            printf("%d\n", t[index]);
    }
    return (EXIT_SUCCESS);
}

