/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on October 18, 2018, 1:39 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int accum(int* array, int length, int init) {
    for (int i = 0; i < length; i++) {
        init += array[i];
    }
    return init;
}

typedef int(*policy)(int, int);

int accum1(int* array, int length, int init,
        policy operation) {
    for (int i = 0; i < length; i++) {
        init = operation(init, array[i]);
    }
    return init;
}

int add(int a, int b) {
    return a + b;
}

int multiply(int a, int b) {
    return a * b;
}


typedef bool(*predicate)(int);

int accum2(int* begin, int* end, int init,
        policy operation,
        predicate condition) {
    for (; begin < end; begin++) {
        if (condition(*begin)) {
            init = operation(init, *begin);
        }
    }
    return init;
}

bool positive(int a)
{
    return a > 0;
}

bool even(int a)
{
    return a % 2 == 0;
}

int my_compare(const void* a1, const void* a2)
{
    return strcmp((const char*)a1, (const char*)a2);
}


bool areAnagrams(const char* s1,
        const char* s2)
{
    bool ret = false;
    if(strlen(s1)!=strlen(s2))
    {
        return ret;
    }
    char* ss1 = malloc(sizeof(char) * (strlen(s1) + 1));
    strcpy(ss1, s1);
    char* ss2 = malloc(sizeof(char) * (strlen(s2) + 1));
    strcpy(ss2, s2);
    qsort(ss1, strlen(ss1), sizeof(char), my_compare);
    qsort(ss2, strlen(ss2), sizeof(char) , my_compare);
    
    if (strcmp(ss1, ss2)==0)
    {
        ret = true;
    }
    
    free(ss1);
    free(ss2);
    
    
}

int main(int argc, char** argv) {

    int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    printf("%d", accum(array, 10, 0));

    printf("%d", accum1(array, 10, 0, add));
    printf("%d", accum1(array, 10, 1, multiply));


    printf("%d", accum2(array, array +10, 0, 
            add, positive));
    printf("%d", accum2(array, array + 10, 1, 
            multiply, even));


    return (EXIT_SUCCESS);
}

