/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "Employee.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Employee
{
    char name[50];
    char title[50];
    int salary;
};

static EmployeePtr database = NULL;
static int size = 0;
static int capacity = 0;

EmployeePtr insert(const char* name, const char* title, int salary)
{
    if(size == capacity)
    {
        capacity = 2 * size + 1;
        EmployeePtr temp = malloc(sizeof(struct Employee)*capacity);
        for(int i = 0; i < size; i++)
        {
            temp[i] = database[i];
        }
        free(database);
        database = temp;
    }
    struct Employee e;
    strcpy(e.name, name);
    strcpy(e.title, title);
    e.salary = salary;
    database[size++] = e;
}

void print(EmployeePtr employee)
{
    printf("[%s,%s,%d]\n", employee->name, employee->title, employee->salary);
}