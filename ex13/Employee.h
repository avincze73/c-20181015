/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Employee.h
 * Author: avincze
 *
 * Created on October 16, 2018, 3:28 PM
 */

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

struct Employee;
typedef struct Employee* EmployeePtr;

EmployeePtr insert(const char* name, const char* title, int salary);
void print(EmployeePtr employee);

#endif /* EMPLOYEE_H */

